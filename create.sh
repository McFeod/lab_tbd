echo "drop database if exists company" | psql -U postgres
echo "create database company; grant all privileges on database company to programmer;" | psql -U postgres
psql -U programmer company < create.sql
psql -U programmer company < alter.sql
psql -U programmer company < extra.sql
psql -U programmer company < trigger.sql
sh insert.sh
psql -U programmer company < views.sql
