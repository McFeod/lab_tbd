-- Бизнес-правила

--======================================================================
-- ненадёжный поставщик не может поставлять дорогие детали
--======================================================================
CREATE FUNCTION check_price_and_rating (supply) RETURNS VOID
	LANGUAGE plpgsql SECURITY DEFINER AS $$
DECLARE
	cost numeric(20, 2);
	producer_rating integer;
BEGIN
	SELECT price INTO cost FROM detail WHERE detail_id = $1.detail_id;
	SELECT rating INTO producer_rating FROM producer
							WHERE producer_id = $1.producer_id;
	IF ((cost >= 1000) AND (producer_rating < 6)) THEN
		RAISE NOTICE 'Поставка % отменена. Причина: %',
					$1.supply_id, 'Поставщик ненадёжен, цена высока';
		DELETE FROM supply WHERE supply_id = $1.supply_id;
	END IF;
END; 
$$;

-- проверка при создании/модификации поставки
CREATE FUNCTION rating_control_supply() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$ 
BEGIN
	PERFORM check_price_and_rating(NEW);
	RETURN NEW;
END; 
$$;

CREATE TRIGGER rating_control_supply_trigger
AFTER INSERT OR UPDATE ON supply FOR EACH ROW 
EXECUTE PROCEDURE rating_control_supply ();

-- проверка при модификации поставщика
CREATE FUNCTION rating_control_producer() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$ 
DECLARE
	cur_supply supply;
BEGIN
	IF (NEW.rating < 6) THEN
		FOR cur_supply IN SELECT * FROM supply
							WHERE producer_id = NEW.producer_id LOOP
			PERFORM check_price_and_rating(cur_supply);
		END LOOP;
	END IF;
	RETURN NEW;
END; 
$$;

CREATE TRIGGER rating_control_producer_trigger
AFTER UPDATE OF rating ON producer FOR EACH ROW 
EXECUTE PROCEDURE rating_control_producer ();

-- проверка при модификации детали
CREATE FUNCTION rating_control_detail() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$ 
DECLARE
	cur_supply supply;
BEGIN
	IF (detail_cost >= 100) THEN
		FOR cur_supply IN SELECT * FROM supply
							WHERE detail_id = NEW.detail_id LOOP
			PERFORM check_price_and_rating(cur_supply);
		END LOOP;
	END IF;
	RETURN NEW;
END; 
$$;

CREATE TRIGGER rating_control_detail_trigger
AFTER UPDATE OF price ON detail FOR EACH ROW 
EXECUTE PROCEDURE rating_control_detail ();

--======================================================================
-- котроль веса поставки (ограничение в 1.5т)
--======================================================================

CREATE FUNCTION check_supply_weight(supply) RETURNS VOID
	 LANGUAGE plpgsql SECURITY DEFINER AS $$ 
DECLARE
	detail_weight integer;
BEGIN
	SELECT weight INTO detail_weight FROM detail 
							WHERE detail_id = $1.detail_id;
	IF (detail_weight * $1.amount > 1000000) THEN
		RAISE NOTICE 'Поставка % отменена. Причина: %',
							$1.supply_id, 'Вес поставки превышает 1.5т';
		DELETE FROM supply WHERE supply_id = $1.supply_id;
	END IF;
END; 
$$;

-- проверка при создании/модификации поставки
CREATE FUNCTION weight_control_supply() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$ 
BEGIN
	PERFORM check_supply_weight(NEW);
	RETURN NEW;
END; 
$$;

CREATE TRIGGER weight_control_supply_trigger
AFTER INSERT OR UPDATE ON supply FOR EACH ROW 
EXECUTE PROCEDURE weight_control_supply ();


-- проверка при модификации детали
CREATE FUNCTION weight_control_detail() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$ 
DECLARE
	cur_supply supply;
BEGIN
	FOR cur_supply IN SELECT * FROM supply
						WHERE detail_id = NEW.detail_id LOOP
		PERFORM check_supply_weight(cur_supply);
	END LOOP;
	RETURN NEW;
END; 
$$;

CREATE TRIGGER weight_control_detail_trigger
AFTER UPDATE OF weight ON detail FOR EACH ROW 
EXECUTE PROCEDURE weight_control_detail ();

--======================================================================
-- Проверка на превышение бюджета проекта
--======================================================================

-- проверка при создании/модификации поставки
CREATE FUNCTION cost_control_supply() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$ 
DECLARE
	project_balance numeric(20, 2);
	sum_cost numeric(20, 2);
BEGIN
	SELECT balance, project_cost 
		INTO project_balance, sum_cost FROM project
							WHERE project_id = NEW.project_id;
	IF (project_balance < sum_cost) THEN
		RAISE NOTICE 'Поставка % отменена. Причина: %',
							NEW.supply_id, 'Недостаточно средств';
		DELETE FROM supply WHERE supply_id = NEW.supply_id;
	END IF;
	RETURN NEW;
END; 
$$;

CREATE TRIGGER cost_control_supply_trigger
AFTER INSERT OR UPDATE ON supply FOR EACH ROW 
EXECUTE PROCEDURE cost_control_supply ();

-- проверка при модификации проекта
CREATE FUNCTION cost_control_project() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$ 
BEGIN
	IF (NEW.balance < NEW.project_cost) THEN
		RAISE NOTICE 'Отмените часть поставок перед уменьшением бюджета';
		RETURN OLD;
	END IF;
	RETURN NEW;
END; 
$$;

CREATE TRIGGER cost_control_project_trigger
BEFORE UPDATE OF balance ON project FOR EACH ROW 
EXECUTE PROCEDURE cost_control_project ();
