--======================================================================
-- экономист
--======================================================================

-- producer
CREATE VIEW "economist_producers_view" AS SELECT
	name AS "поставщик",
	city AS "город",
	address AS "адрес",
	rating AS "рейтинг", 
	CASE 
		WHEN rating > 5 THEN 'надёжный'
		ELSE 'ненадёжный'
	END AS "надёжность"
FROM producer ORDER BY city, name, rating DESC;

-- detail
CREATE VIEW "economist_details_view" AS SELECT
	name AS "деталь",
	to_char(price/1000, '99999990.99999') AS "цена",
	color AS "цвет",
	weight/1000 AS "вес",
	CASE
		WHEN price < 1000 THEN 'дешёвая'
		ELSE 'дорогая'
	END AS "оценка стоимости"
FROM detail ORDER BY price DESC, name, color, weight ASC;

--project
CREATE VIEW "economist_projects_view" AS SELECT
	name AS "проект",
	city AS "город",
	address AS "адрес",
	balance AS "бюджет"
FROM project ORDER BY city, name, balance DESC;

-- supply
CREATE VIEW "economist_supplies_view" AS SELECT
	detail.name AS "деталь",
	detail.color AS "цвет",
	CASE
		WHEN detail.price < 1000 THEN 'дешёвая'
		ELSE 'дорогая'
	END AS "оценка стоимости",
	producer.name AS "поставщик",
	producer.city AS "город",
	CASE 
		WHEN rating > 5 THEN 'надёжный'
		ELSE 'ненадёжный'
	END AS "надёжность",
	amount AS "количество деталей",
	(detail.weight * amount)/1000 AS "вес",
	(supply_cost / 1000) :: numeric(20, 5) AS "стоимость"
FROM supply
LEFT JOIN detail ON supply.detail_id = detail.detail_id
LEFT JOIN producer ON supply.producer_id = producer.producer_id
LEFT JOIN project ON supply.project_id = project.project_id
ORDER BY "деталь", "поставщик", "стоимость" DESC, "вес" DESC;
--======================================================================
-- директор
--======================================================================

-- producer
CREATE VIEW "director_producers_view" AS SELECT
	name AS "поставщик",
	city AS "город",
	CASE 
		WHEN rating > 5 THEN 'надёжный'
		ELSE 'ненадёжный'
	END AS "надёжность"
FROM producer ORDER BY "надёжность", city, name DESC;

-- detail
CREATE VIEW "director_details_view" AS SELECT
	name AS "деталь",
	to_char(price/1000, '99999990.99999') AS "цена",
	color AS "цвет",
	CASE
		WHEN price < 1000 THEN 'дешёвая'
		ELSE 'дорогая'
	END AS "оценка стоимости"
FROM detail ORDER BY price DESC;

--project
CREATE VIEW "director_projects_view" AS SELECT
	name AS "проект",
	city AS "город",
	balance AS "бюджет"
FROM project ORDER BY balance DESC, city, name;

-- supply
CREATE VIEW "director_supplies_view" AS SELECT
	detail.name AS "деталь",
	detail.color AS "цвет",
	CASE
		WHEN detail.price < 1000 THEN 'дешёвая'
		ELSE 'дорогая'
	END AS "оценка стоимости",
	producer.name AS "поставщик",
	producer.city AS "город",
	CASE 
		WHEN rating > 5 THEN 'надёжный'
		ELSE 'ненадёжный'
	END AS "надёжность",
	amount AS "количество деталей",
	(detail.weight * amount)/1000 AS "вес",
	(supply_cost / 1000) :: numeric(20, 5) AS "стоимость"
FROM supply
LEFT JOIN detail ON supply.detail_id = detail.detail_id
LEFT JOIN producer ON supply.producer_id = producer.producer_id
LEFT JOIN project ON supply.project_id = project.project_id
ORDER BY "оценка стоимости", "надёжность", "город", "деталь",
	"поставщик", "стоимость" DESC, "вес" DESC;
