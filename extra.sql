ALTER TABLE supply ADD COLUMN supply_cost numeric(20, 2) DEFAULT 0;
ALTER TABLE project ADD COLUMN project_cost numeric(20, 2) DEFAULT 0;

-- Подсчёт стоимости поставки и суммарной стоимости поставок в проекте
CREATE FUNCTION complete_supply() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$
DECLARE
	detail_cost numeric(20, 2);
	count integer;
	id integer;
	yn char;
BEGIN
	-- дополняем поставку
	SELECT price INTO detail_cost FROM detail
							WHERE detail_id = NEW.detail_id;
	NEW.supply_cost = NEW.amount * detail_cost;
	-- дополняем проект
	IF TG_OP = 'INSERT' THEN
		UPDATE project
		SET project_cost = project_cost + NEW.supply_cost
		WHERE project_id = NEW.project_id;
	ELSE
		UPDATE project
		SET project_cost = project_cost + NEW.supply_cost - OLD.supply_cost
		WHERE project_id = NEW.project_id;
	END IF;
	return NEW;
END; 
$$;

CREATE TRIGGER costs_after_insert 
BEFORE INSERT OR UPDATE OF amount, detail_id, supply_cost
							ON supply FOR EACH ROW 
EXECUTE PROCEDURE complete_supply();

-- Пересчёт при изменении цен
CREATE FUNCTION update_supply() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$
DECLARE
	id integer;
	prj integer;
	old_cost numeric(20, 2);
	new_cost numeric(20, 2);
	count integer;
BEGIN
	FOR id, count, prj, old_cost IN 
		SELECT supply_id, amount, project_id, supply_cost FROM supply 
							WHERE detail_id = NEW.detail_id LOOP
		new_cost = NEW.price * count;
		-- для поставки
		UPDATE supply
		SET supply_cost = new_cost
		WHERE supply_id = id;
		-- для проекта
		UPDATE project
		SET project_cost = project_cost + new_cost - old_cost
		WHERE project_id = prj;
	END LOOP;
	return NEW;
END; 
$$;

CREATE TRIGGER costs_on_price_update 
BEFORE UPDATE OF price ON detail FOR EACH ROW 
EXECUTE PROCEDURE update_supply();

-- если кому-то придёт в голову сменить проект поставки
CREATE FUNCTION change_supply_project() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$
DECLARE
	id integer;
	prj integer;
	old_cost numeric(20, 2);
	new_cost numeric(20, 2);
	count integer;
BEGIN
	UPDATE project
	SET project_cost = project_cost + NEW.supply_cost
	WHERE project_id = NEW.project_id;
	UPDATE project
	SET project_cost = project_cost - NEW.supply_cost
	WHERE project_id = OLD.project_id;
	return NEW;
END; 
$$;

CREATE TRIGGER on_supply_project_change
BEFORE UPDATE OF project_id ON supply FOR EACH ROW 
EXECUTE PROCEDURE change_supply_project();

-- при удалении поставки "возвращаем деньги" в проект
CREATE FUNCTION restore_project_cost() RETURNS trigger
	LANGUAGE plpgsql SECURITY DEFINER AS $$
DECLARE
	id integer;
	prj integer;
	old_cost numeric(20, 2);
	new_cost numeric(20, 2);
	count integer;
BEGIN
	UPDATE project
	SET project_cost = project_cost - OLD.supply_cost
	WHERE project_id = OLD.project_id;
	return OLD;
END; 
$$;

CREATE TRIGGER on_delete_supply
BEFORE DELETE ON supply FOR EACH ROW 
EXECUTE PROCEDURE restore_project_cost();


UPDATE supply SET amount = amount;
