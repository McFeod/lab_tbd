CREATE OR REPLACE FUNCTION print(message text) RETURNS void AS $$
BEGIN
    RAISE INFO 
'--------------------------- % ------------------------------', message;
END;
$$ LANGUAGE plpgsql;

--======================================================================
SELECT print('Запросы экономиста');
--======================================================================

SELECT print('список всех поставщиков');
SELECT * FROM economist_producers_view;
------------------------------------------------------------------------

SELECT print('список надёжных поставщиков');
SELECT * FROM economist_producers_view
WHERE "надёжность"='надёжный';
------------------------------------------------------------------------

SELECT print('список ненадёжных поставщиков');
SELECT * FROM economist_producers_view
WHERE "надёжность"='ненадёжный';
------------------------------------------------------------------------

SELECT print('список всех деталей');
SELECT * FROM economist_details_view;
------------------------------------------------------------------------

SELECT print('список дешёвых деталей');
SELECT * FROM economist_details_view
WHERE "оценка стоимости"='дешёвая';
------------------------------------------------------------------------

SELECT print('список дорогих деталей');
SELECT * FROM economist_details_view
WHERE "оценка стоимости"='дорогая';
------------------------------------------------------------------------

SELECT print('список всех проектов');
SELECT * FROM economist_projects_view;
------------------------------------------------------------------------

SELECT print('список проектов из заданного города');
SELECT * FROM economist_projects_view
WHERE "город"='Брянск';
------------------------------------------------------------------------

SELECT print('список проектов с бюджетом в заданном диапазоне');
SELECT * FROM economist_projects_view
WHERE "бюджет" BETWEEN 10000 and 100000;
------------------------------------------------------------------------

SELECT print('список всех поставок');
SELECT * FROM economist_supplies_view;
------------------------------------------------------------------------

SELECT print('список поставок дешёвых деталей');
SELECT * FROM economist_supplies_view
WHERE "оценка стоимости"='дешёвая';
------------------------------------------------------------------------

SELECT print('список поставок дорогих деталей');
SELECT * FROM economist_supplies_view
WHERE "оценка стоимости"='дорогая';
------------------------------------------------------------------------

SELECT print('список поставок от надёжных поставщиков');
SELECT * FROM economist_supplies_view
WHERE "надёжность"='надёжный';
------------------------------------------------------------------------

SELECT print('список поставок от ненадёжных поставщиков');
SELECT * FROM economist_supplies_view
WHERE "надёжность"='ненадёжный';
------------------------------------------------------------------------

SELECT print('список поставок со стоимостью из заданного диапазона');
--SELECT * FROM economist_supplies_view
--WHERE "стоимость" BETWEEN 10 and 100;
------------------------------------------------------------------------

--======================================================================
SELECT print('Запросы директора');
--======================================================================

SELECT print('список всех поставщиков с указанием стоимости поставок');
SELECT name AS "поставщик", 
	city AS "город",
	CASE 
		WHEN rating > 5 THEN 'надёжный'
		ELSE 'ненадёжный'
	END AS "надёжность",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) 
		AS "стоимость поставок, тыс. р."
FROM producer LEFT JOIN supply 
	ON producer.producer_id = supply.producer_id
GROUP BY producer.producer_id;
------------------------------------------------------------------------

SELECT print('список поставщиков с рейтингом не ниже среднего');
SELECT name AS "поставщик", 
	city AS "город",
	CASE 
		WHEN rating > 5 THEN 'надёжный'
		ELSE 'ненадёжный'
	END AS "надёжность",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) AS "стоимость поставок, тыс. р."
FROM producer LEFT JOIN supply 
	ON producer.producer_id = supply.producer_id
WHERE rating >= (SELECT AVG(rating) FROM producer)
GROUP BY producer.producer_id;
------------------------------------------------------------------------

SELECT print('список поставщиков с рейтингом не выше среднего');
SELECT name AS "поставщик", 
	city AS "город",
	CASE 
		WHEN rating > 5 THEN 'надёжный'
		ELSE 'ненадёжный'
	END AS "надёжность",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) AS "стоимость поставок, тыс. р."
FROM producer LEFT JOIN supply 
	ON producer.producer_id = supply.producer_id
WHERE rating <= (SELECT AVG(rating) FROM producer)
GROUP BY producer.producer_id;
------------------------------------------------------------------------

SELECT print('список поставщиков с максимальным рейтингом');
SELECT name AS "поставщик", 
	city AS "город",
	CASE 
		WHEN rating > 5 THEN 'надёжный'
		ELSE 'ненадёжный'
	END AS "надёжность",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) AS "стоимость поставок, тыс. р."
FROM producer LEFT JOIN supply 
	ON producer.producer_id = supply.producer_id
WHERE rating = (SELECT MAX(rating) FROM producer)
GROUP BY producer.producer_id;
------------------------------------------------------------------------

SELECT print('список поставщиков с минимальным рейтингом');
SELECT name AS "поставщик", 
	city AS "город",
	CASE 
		WHEN rating > 5 THEN 'надёжный'
		ELSE 'ненадёжный'
	END AS "надёжность",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) AS "стоимость поставок, тыс. р."
FROM producer LEFT JOIN supply 
	ON producer.producer_id = supply.producer_id
WHERE rating = (SELECT MIN(rating) FROM producer)
GROUP BY producer.producer_id;
------------------------------------------------------------------------

SELECT print('список всех деталей');
SELECT
	name AS "деталь",
	(price/1000):: numeric(20, 5) AS "цена, тыс. р.",
	color AS "цвет",
	CASE
		WHEN price < 1000 THEN 'дешёвая'
		ELSE 'дорогая'
	END AS "оценка стоимости",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) AS "стоимость поставок, тыс. р."
FROM detail LEFT JOIN supply ON detail.detail_id = supply.detail_id
GROUP BY detail.detail_id;
------------------------------------------------------------------------

SELECT print('список деталей с ценой не выше средней');
SELECT
	name AS "деталь",
	(price/1000):: numeric(20, 5) AS "цена, тыс. р.",
	color AS "цвет",
	CASE
		WHEN price < 1000 THEN 'дешёвая'
		ELSE 'дорогая'
	END AS "оценка стоимости",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) AS "стоимость поставок, тыс. р."
FROM detail LEFT JOIN supply ON detail.detail_id = supply.detail_id
WHERE price <= (SELECT AVG(price) FROM detail)
GROUP BY detail.detail_id;
------------------------------------------------------------------------

SELECT print('список деталей с ценой не ниже средней');
SELECT
	name AS "деталь",
	(price/1000):: numeric(20, 5) AS "цена, тыс. р.",
	color AS "цвет",
	CASE
		WHEN price < 1000 THEN 'дешёвая'
		ELSE 'дорогая'
	END AS "оценка стоимости",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) AS "стоимость поставок, тыс. р."
FROM detail LEFT JOIN supply ON detail.detail_id = supply.detail_id
WHERE price >= (SELECT AVG(price) FROM detail)
GROUP BY detail.detail_id;
------------------------------------------------------------------------

SELECT print('список деталей с максимальной ценой');
SELECT
	name AS "деталь",
	(price/1000):: numeric(20, 5) AS "цена, тыс. р.",
	color AS "цвет",
	CASE
		WHEN price < 1000 THEN 'дешёвая'
		ELSE 'дорогая'
	END AS "оценка стоимости",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) AS "стоимость поставок, тыс. р."
FROM detail LEFT JOIN supply ON detail.detail_id = supply.detail_id
WHERE price = (SELECT MAX(price) FROM detail)
GROUP BY detail.detail_id;
------------------------------------------------------------------------

SELECT print('список деталей с минимальной ценой');
SELECT
	name AS "деталь",
	(price/1000):: numeric(20, 5) AS "цена, тыс. р.",
	color AS "цвет",
	CASE
		WHEN price < 1000 THEN 'дешёвая'
		ELSE 'дорогая'
	END AS "оценка стоимости",
	(COALESCE(SUM(supply_cost), 0)/1000) :: numeric(20, 5) AS "стоимость поставок, тыс. р."
FROM detail LEFT JOIN supply ON detail.detail_id = supply.detail_id
WHERE price = (SELECT MIN(price) FROM detail)
GROUP BY detail.detail_id;
------------------------------------------------------------------------

SELECT print('список всех проектов');
SELECT
	name AS "проект",
	city AS "город",
	balance AS "бюджет",
	project_cost AS "стоимость поставок, тыс. р."
FROM project;
------------------------------------------------------------------------

SELECT print('список проектов с бюждетом не ниже среднего');
SELECT
	name AS "проект",
	city AS "город",
	balance AS "бюджет",
	project_cost AS "стоимость поставок, тыс. р."
FROM project
WHERE balance >= (SELECT AVG(balance) FROM project);
------------------------------------------------------------------------

SELECT print('список проектов с бюждетом не выше среднего');
SELECT
	name AS "проект",
	city AS "город",
	balance AS "бюджет",
	project_cost AS "стоимость поставок, тыс. р."
FROM project
WHERE balance <= (SELECT AVG(balance) FROM project);
------------------------------------------------------------------------

SELECT print('список проектов с максимальным бюждетом');
SELECT
	name AS "проект",
	city AS "город",
	balance AS "бюджет",
	project_cost AS "стоимость поставок, тыс. р."
FROM project
WHERE balance = (SELECT MAX(balance) FROM project);
------------------------------------------------------------------------

SELECT print('список проектов с минимальным бюждетом');
SELECT
	name AS "проект",
	city AS "город",
	balance AS "бюджет",
	project_cost AS "стоимость поставок, тыс. р."
FROM project
WHERE balance = (SELECT MIN(balance) FROM project);
------------------------------------------------------------------------

SELECT SUM(supply_cost) AS "Сумма всех поставок" FROM supply;

------------------------------------------------------------------------

SELECT SUM(supply_cost) AS "Сумма поставок дорогих деталей"
FROM supply LEFT JOIN detail ON supply.detail_id = detail.detail_id
WHERE detail.price >= 1000;

------------------------------------------------------------------------

SELECT SUM(supply_cost) AS "Сумма поставок дешёвых деталей"
FROM supply LEFT JOIN detail ON supply.detail_id = detail.detail_id
WHERE detail.price < 1000;

------------------------------------------------------------------------
SELECT print('Сумма поставок от надёжных поставщиков');
SELECT SUM(supply_cost) AS "Сумма поставок"
FROM supply LEFT JOIN producer ON supply.producer_id = producer.producer_id
WHERE producer.rating > 5;

------------------------------------------------------------------------
SELECT print('Сумма поставок от ненадёжных поставщиков');
SELECT SUM(supply_cost) AS "Сумма поставок"
FROM supply LEFT JOIN producer ON supply.producer_id = producer.producer_id
WHERE producer.rating <= 5;

------------------------------------------------------------------------

SELECT SUM(supply_cost) AS "Сумма поставок от поставщика 1"
FROM supply
WHERE producer_id = 1;

------------------------------------------------------------------------
