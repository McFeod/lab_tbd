REVOKE ALL ON ALL TABLES IN SCHEMA PUBLIC FROM economist, director, reader, 
detail_boss, producer_boss, project_boss, supply_boss, supply_censor;

DROP ROLE IF EXISTS economist, director, reader, detail_boss, 
producer_boss, project_boss, supply_boss, supply_censor;

CREATE ROLE economist LOGIN;
CREATE ROLE director LOGIN;

CREATE ROLE reader;
GRANT SELECT ON supply, detail, project, producer TO reader;

CREATE ROLE detail_boss;
GRANT INSERT, UPDATE, DELETE
ON detail TO detail_boss;

CREATE ROLE producer_boss;
GRANT INSERT, UPDATE, DELETE
ON producer TO producer_boss;

CREATE ROLE project_boss;
GRANT INSERT, UPDATE, DELETE
ON project TO project_boss;

CREATE ROLE supply_boss;
GRANT INSERT, UPDATE, DELETE
ON supply TO supply_boss;

CREATE ROLE supply_censor;
GRANT DELETE ON supply TO supply_censor;

GRANT reader, producer_boss, supply_boss TO economist;

GRANT reader, producer_boss, project_boss, detail_boss, supply_censor
TO director;

GRANT SELECT ON economist_producers_view, economist_details_view, 
economist_supplies_view, economist_projects_view TO economist;

GRANT SELECT ON director_details_view, director_producers_view,
director_projects_view, director_supplies_view TO director;
