--======================================================================
-- Целостность атрибутов
--======================================================================
-- not null
ALTER TABLE detail ALTER COLUMN name SET NOT NULL;
ALTER TABLE detail ALTER COLUMN price SET NOT NULL;
ALTER TABLE producer ALTER COLUMN name SET NOT NULL;
ALTER TABLE producer ALTER COLUMN city SET NOT NULL;
ALTER TABLE producer ALTER COLUMN address SET NOT NULL;
ALTER TABLE producer ALTER COLUMN rating SET NOT NULL;
ALTER TABLE project ALTER COLUMN name SET NOT NULL;
ALTER TABLE project ALTER COLUMN city SET NOT NULL;
ALTER TABLE project ALTER COLUMN address SET NOT NULL;
ALTER TABLE project ALTER COLUMN balance SET NOT NULL;
ALTER TABLE supply ALTER COLUMN amount SET NOT NULL;

-- числовые ограничения
ALTER TABLE detail ADD CONSTRAINT positive_price CHECK (price > 0);
ALTER TABLE detail ADD CONSTRAINT positive_weight CHECK (weight > 0);
ALTER TABLE supply ADD CONSTRAINT positive_amount CHECK (amount > 0);
ALTER TABLE project ADD CONSTRAINT positive_balance CHECK (balance > 0);
ALTER TABLE producer ADD CONSTRAINT ten_rating CHECK
                            ((rating > 0) AND (rating <= 10));

-- имя поставщика уникально для города
ALTER TABLE producer ADD CONSTRAINT name_in_city UNIQUE (city, name);

-- ограничение на цвета
CREATE TYPE possible_color AS ENUM('белый', 'чёрный', 'красный',
'синий', 'серый', 'зелёный', 'жёлтый', 'оранжевый', 'коричневый');

ALTER TABLE detail ALTER COLUMN color TYPE possible_color
USING color::possible_color;

--======================================================================
-- Ссылочная целостность
--======================================================================
ALTER TABLE supply ADD CONSTRAINT detail_cascade 
FOREIGN KEY (detail_id)
REFERENCES detail(detail_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE supply ADD CONSTRAINT producer_cascade 
FOREIGN KEY (producer_id)
REFERENCES producer(producer_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE supply ADD CONSTRAINT project_cascade
FOREIGN KEY (project_id)
REFERENCES project(project_id) ON DELETE CASCADE ON UPDATE CASCADE;
