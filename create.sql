-- Деталь
CREATE TABLE "detail" (
    "detail_id" serial NOT NULL PRIMARY KEY,   -- идентификатор
    "name" varchar(64),                 -- название
    "price" numeric(20, 2),            -- стоимость
    "color" varchar(64),                -- цвет
    "weight" float                      -- вес (в граммах)
)
;
-- Поставщик
CREATE TABLE "producer" (
    "producer_id" serial NOT NULL PRIMARY KEY,   -- идентификатор
    "name" varchar(64),                 -- название
    "city" varchar(64),                 -- город
    "address" varchar(64),              -- адрес
    "rating" integer                    -- рейтинг
)
;
-- Проект
CREATE TABLE "project" (
    "project_id" serial NOT NULL PRIMARY KEY,   -- идентификатор
    "name" varchar(64),                 -- название
    "city" varchar(64),                 -- город
    "address" varchar(64),              -- адрес
    "balance" numeric(20, 2)            -- бюджет
)
;
-- Поставка
CREATE TABLE "supply" (
    "supply_id" serial NOT NULL PRIMARY KEY,   -- идентификатор
    "project_id" integer NOT NULL REFERENCES "project" ("project_id"),
                                        -- идентификатор проекта
    "amount" integer,                   -- число деталей в поставке
    "detail_id" integer NOT NULL REFERENCES "detail" ("detail_id"),
                                        -- идентификатор детали
    "producer_id" integer NOT NULL REFERENCES "producer" ("producer_id")
                                        -- идентификатор поставщика
)
;
